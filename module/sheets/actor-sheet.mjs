import {onManageActiveEffect, prepareActiveEffectCategories} from "../helpers/effects.mjs";
import {BarrelRollApp} from "../apps.mjs";
import { ManeuverSelection } from "../apps.mjs";

/**
 * Extend the basic ActorSheet with some very simple modifications
 * @extends {ActorSheet}
 */
export class XwingActorSheet extends ActorSheet {

  /** @override */
  static get defaultOptions() {
    return mergeObject(super.defaultOptions, {
      classes: ["xwing", "sheet", "actor"],
      template: "systems/xwing/templates/actor/actor-sheet.html",
      width: 600,
      height: 600,
      tabs: [{ navSelector: ".sheet-tabs", contentSelector: ".sheet-body", initial: "items" }]
    });
  }

  /** @override */
  get template() {
    return `systems/xwing/templates/actor/actor-${this.actor.type}-sheet.html`;
  }

  /** @inheritdoc */
  async _preCreate(data, options, user) {
    await super._preCreate(data, options, user);



    //dnd5e code for example
    /**
    const sourceId = this.getFlag("core", "sourceId");
    if ( sourceId?.startsWith("Compendium.") ) return;

    // Some sensible defaults for convenience
    // Token size category
    const s = CONFIG.DND5E.tokenSizes[this.data.data.traits.size || "med"];
    this.data.token.update({width: s, height: s});

    // Player character configuration
    if ( this.type === "character" ) {
      this.data.token.update({vision: true, actorLink: true, disposition: 1});
    } */
  }


  /* -------------------------------------------- */

  /** @override */
  getData() {
    // Retrieve the data structure from the base sheet. You can inspect or log
    // the context variable to see the structure, but some key properties for
    // sheets are the actor object, the data object, whether or not it's
    // editable, the items array, and the effects array.
    const context = super.getData();

    // Use a safe clone of the actor data for further operations.
    const actor = context.actor;

    // Add the actor's data to context.data for easier access, as well as flags.
    context.system = actor.system;
    console.log(context.system)
    context.flags = actor.flags;

    // Prepare ship data and items.
    if (actor.type == 'ship') {
      //this._prepareItems(context);
      this._prepareUpgrades(context);
      //this._prepareCharacterData(context);
    }
    // Prepare character data and items.
    if (actor.type == 'character') {
      this._prepareItems(context);
      this._prepareCharacterData(context);
    }

    // Prepare NPC data and items.
    if (actor.type == 'npc') {
      this._prepareItems(context);
    }

    // Add roll data for TinyMCE editors.
    context.rollData = context.actor.getRollData();

    // Prepare active effects
    context.effects = prepareActiveEffectCategories(this.actor.effects);

    //context.icons = CONFIG.XWING.icons;
    context.icons = CONFIG.XWING.icons;

    if (context.document?.parent?.combatant) {
      let combatant = context.document.parent.combatant;
      context.tokens = {
        "stress": combatant.getFlag("xwing", "stress") ?? 0,
        "focus": combatant.getFlag("xwing", "focus") ?? 0,
        "evade": combatant.getFlag("xwing", "evade") ?? 0,
      };
    } else {
      context.tokens = {
        "stress": 0,
        "focus": 0,
        "evade": 0,
      };
    }

    context.model_label = game.i18n.localize(CONFIG.XWING.ship_names[context.system.model]) ?? context.system.model;

    //Maneuver list for maneuvers tab
    context.columns = CONFIG.XWING.maneuvers.columns;
    context.speeds = CONFIG.XWING.maneuvers.speeds;
    context.show = context.system.maneuvers;
    context.action_description = 'view';
    
    return context;
  }

  /**
   * Organize and classify Items for Character sheets.
   *
   * @param {Object} actorData The actor to prepare.
   *
   * @return {undefined}
   */
  _prepareCharacterData(context) {
    // Handle ability scores.
    for (let [k, v] of Object.entries(context.data.abilities)) {
      v.label = game.i18n.localize(CONFIG.XWING.abilities[k]) ?? k;
    }
  }


  /**
   * Organize and classify Items for Character sheets.
   *
   * @param {Object} actorData The actor to prepare.
   *
   * @return {undefined}
   */
  _prepareUpgrades(context) {
    const upgrade_slots = ['pilot', 'title', ...context.system.upgrade_slots, 'modification'];
    //const upgrade_slots = context.data.upgrade_slots; // ['foo', 'foo', 'bar', 'bat', 'foo']
    const items = context.items;

    const displayItems = [...new Set(upgrade_slots)].reduce(
      (acc, upgradeSlot) => {
        // this might be a collection instead of an array
        const relevantItems = items.filter(({type}) => type === upgradeSlot);
        const upgradeSlotMax = upgrade_slots.filter((slot) => slot === upgradeSlot).length;
        // mutate accumulator
        acc[upgradeSlot] = {
          name: upgradeSlot,
          label: game.i18n.localize(CONFIG.XWING.upgrade_names[upgradeSlot]) ?? upgradeSlot,
          icon: CONFIG.XWING.icons.upgrades[upgradeSlot],
          items: relevantItems,
          count: relevantItems.length,
          max: upgradeSlotMax,
        }
        return acc;
      },
      {}
    );

    context.maybe_hide = ['pilot', 'title', 'modification'];

    displayItems.pilot.icon = CONFIG.XWING.icons.faction_helm[context.system.faction];

    const upgradePoints = items.reduce((total,item) => total + item.system.points, 0);

    context.upgrades = displayItems;
    context.upgradePoints = upgradePoints;
    context.totalPoints = context.system.points + upgradePoints;
  }

  /**
   * Organize and classify Items for Character sheets.
   *
   * @param {Object} actorData The actor to prepare.
   *
   * @return {undefined}
   */
  _prepareItems(context) {
    // Initialize containers.
    const gear = [];
    const features = [];
    const spells = {
      0: [],
      1: [],
      2: [],
      3: [],
      4: [],
      5: [],
      6: [],
      7: [],
      8: [],
      9: []
    };

    // Iterate through items, allocating to containers
    for (let i of context.items) {
      i.img = i.img || DEFAULT_TOKEN;
      // Append to gear.
      if (i.type === 'item') {
        gear.push(i);
      }
      // Append to features.
      else if (i.type === 'feature') {
        features.push(i);
      }
      // Append to spells.
      else if (i.type === 'spell') {
        if (i.data.spellLevel != undefined) {
          spells[i.data.spellLevel].push(i);
        }
      }
    }

    // Assign and return
    context.gear = gear;
    context.features = features;
    context.spells = spells;
   }

  /* -------------------------------------------- */

  /** @override */
  activateListeners(html) {
    super.activateListeners(html);

    // Render the item sheet for viewing/editing prior to the editable check.
    html.find('.item-edit').click(ev => {
      const li = $(ev.currentTarget).parents(".item");
      const item = this.actor.items.get(li.data("itemId"));
      item.sheet.render(true);
    });

    // -------------------------------------------------------------
    // Everything below here is only needed if the sheet is editable
    if (!this.isEditable) return;

    // Add Inventory Item
    html.find('.item-create').click(this._onItemCreate.bind(this));

    // Delete Inventory Item
    html.find('.item-delete').click(ev => {
      const li = $(ev.currentTarget).parents(".item");
      const item = this.actor.items.get(li.data("itemId"));
      item.delete();
      li.slideUp(200, () => this.render(false));
    });

    // Active Effect management
    html.find(".effect-control").click(ev => onManageActiveEffect(ev, this.actor));

    // Rollable abilities.
    html.find('.rollable').click(this._onRoll.bind(this));

    // Drag events for macros.
    if (this.actor.isOwner) {
      let handler = ev => this._onDragStart(ev);
      html.find('li.item').each((i, li) => {
        if (li.classList.contains("inventory-header")) return;
        li.setAttribute("draggable", true);
        li.addEventListener("dragstart", handler, false);
      });
    }

    // Listen for someone taking the Barrel Roll action
    html.find('.action-barrel_roll').click(async ev => {
      if (this.document?.parent) {
        new BarrelRollApp(this.document.parent).render(true);
      } else {
        ui.notifications.info("Can't Barrel Roll because the actor doesn't have a token");
      };
    });

    // Listen for someone taking the Boost action
    html.find('.action-boost').click(async ev => {
      if (this.document?.parent) {
        new ManeuverSelection(this.document.parent, "custom", {'1s': 'w', '1br': 'w', '1bl': 'w'}).render(true);
      } else {
        ui.notifications.info("Can't Boost because the actor doesn't have a token");
      };
    });

    // Listen for someone taking the Evade action
    html.find('.action-evade').click(async ev => {
      if (this.document?.parent?.combatant) {
        await this.document.parent.combatant.setFlag("xwing", "evade", (this.document.parent.combatant.flags?.xwing?.evade ?? 0)+1);
        this.render();
      } else {
        ui.notifications.info("Can't Evade because the actor is not in combat");
      };
    });

    // Listen for someone taking the Focus action
    html.find('.action-focus').click(async ev => {
      if (this.document?.parent?.combatant) {
        await this.document.parent.combatant.setFlag("xwing", "focus", (this.document.parent.combatant.flags?.xwing?.focus ?? 0)+1);
        this.render();
      } else {
        ui.notifications.info("Can't Focus because the actor is not in combat");
      };
    });

    // Listen for someone using Focus
    html.find('.spend-focus').click(async ev => {
      if (this.document?.parent?.combatant) {
        await this.document.parent.combatant.setFlag("xwing", "focus", Math.max(0,(this.document.parent.combatant.flags?.xwing?.focus ?? 0)-1));
        this.render();
      } else {
        ui.notifications.info("Can't Focus because the actor is not in combat");
      };
    });

    // Listen for someone taking the Evade action
    html.find('.spend-evade').click(async ev => {
      if (this.document?.parent?.combatant) {
        await this.document.parent.combatant.setFlag("xwing", "evade", Math.max(0,(this.document.parent.combatant.flags?.xwing?.evade ?? 0)-1));
        this.render();
      } else {
        ui.notifications.info("Can't Evade because the actor is not in combat");
      };
    });
  }

  /**
   * Handle creating a new Owned Item for the actor using initial data defined in the HTML dataset
   * @param {Event} event   The originating click event
   * @private
   */
  async _onItemCreate(event) {
    event.preventDefault();
    const header = event.currentTarget;
    // Get the type of item to create.
    const type = header.dataset.type;
    // Grab any data associated with this control.
    const data = duplicate(header.dataset);
    // Initialize a default name.
    const name = `New ${type.capitalize()}`;
    // Prepare the item object.
    const itemData = {
      name: name,
      type: type,
      data: data
    };
    // Remove the type from the dataset since it's in the itemData.type prop.
    delete itemData.data["type"];

    // Finally, create the item!
    return await Item.create(itemData, {parent: this.actor});
  }

  /**
   * Handle clickable rolls.
   * @param {Event} event   The originating click event
   * @private
   */
  _onRoll(event) {
    event.preventDefault();
    const element = event.currentTarget;
    const dataset = element.dataset;

    // Handle item rolls.
    if (dataset.rollType) {
      if (dataset.rollType == 'item') {
        const itemId = element.closest('.item').dataset.itemId;
        const item = this.actor.items.get(itemId);
        if (item) return item.roll();
      }
    }

    // Handle rolls that supply the formula directly.
    if (dataset.roll) {
      let label = dataset.label ? `[ability] ${dataset.label}` : '';
      let roll = new Roll(dataset.roll, this.actor.getRollData()).roll();
      roll.toMessage({
        speaker: ChatMessage.getSpeaker({ actor: this.actor }),
        flavor: label,
        rollMode: game.settings.get('core', 'rollMode'),
      });
      return roll;
    }
  }

}


/**
 * Extend the basic ActorSheet with some very simple modifications
 * @extends {ActorSheet}
 */
 export class XwingEditSheet extends ActorSheet {

  /** @override */
  static get defaultOptions() {
    return mergeObject(super.defaultOptions, {
      classes: ["xwing", "sheet", "actor"],
      template: "systems/xwing/templates/actor/actor-sheet.html",
      width: 600,
      height: 600,
      tabs: [{ navSelector: ".sheet-tabs", contentSelector: ".sheet-body", initial: "items" }]
    });
  }

  /** @override */
  get template() {
    return `systems/xwing/templates/actor/actor-edit-sheet.html`;
  }

  /** @override */
  get title() {
    return `[${game.i18n.format("XWING.EditModeTitle")}] ${super.title}`;
  }
  
  /** @override */
  getData() {
    const context = super.getData();
    const actorData = context.actor;
    context.system = actorData.system;
    context.flags = actorData.flags;
    context.rollData = context.actor.getRollData();
    context.effects = prepareActiveEffectCategories(this.actor.effects);

    context.icons = CONFIG.XWING.icons;
    context.action_choices = Object.keys(CONFIG.XWING.icons.actions);
    context.upgrade_choices = CONFIG.XWING.upgrade_names;

    context.ship_models = CONFIG.XWING.ship_names;
    context.factions = CONFIG.XWING.factions;
    return context;
  }

  /** @override */
  activateListeners(html) {

    html.find('.assign-maneuvers').click(async ev => {
      new ManeuverSelection({'actor': this.document}, "program").render(true);
      
    });

    //Cycle action selection
    html.find('.action-selection').click(async ev => {
      if (ev.currentTarget.classList.contains('b')) {
        ev.currentTarget.classList.remove('b');
        ev.currentTarget.classList.add('g-border');
        
      } else if (ev.currentTarget.classList.contains('g-border')) {
        ev.currentTarget.classList.remove('g-border');
        ev.currentTarget.classList.add('b');                
    }
    let selected_html = [...html.find(".g-border")]
    let selected = selected_html.reduce((a,x) => {
      a.push(x.dataset.action);
      return a;
    }, []);
    await this.document.update({"data.actions": selected}, {render: false});
    });

    html.find('.add-upgrade').click(async ev => {
      let to_add = html.find('.upgrade-type').val();
      let upgrades = this.document.system.upgrade_slots;
      upgrades.push(to_add);
      await this.document.update({"data.upgrade_slots": upgrades.sort()});
    });

    html.find('.del-upgrade').click(async ev =>{
      let to_remove = ev.currentTarget.dataset.upgrade;
      let upgrades = this.document.system.upgrade_slots;
      upgrades.splice(upgrades.indexOf(to_remove),1)
      await this.document.update({"data.upgrade_slots": upgrades.sort()});
    });

  }
 }