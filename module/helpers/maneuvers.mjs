export async function executeManeuver(token, maneuver) {
    if (token?.document) token = token.document;
    let {color, speed, shape}  = maneuver.match(/(?<color>[grw])(?<speed>[0-9])(?<shape>[a-z]{1,2})/).groups;

    if (color =='r'){
        await token.combatant.setFlag("xwing", "stress", (token.combatant.flags?.xwing?.stress ?? 0)+1);
        token.actor.render();
    }
    else if (color == 'g') {
        await token.combatant.setFlag("xwing", "stress", Math.max(0, (token.combatant.flags?.xwing?.stress ?? 0)-1));
        token.actor.render();
    }   

    let update = {}
    if (speed === '0'){ return; }
    else if (shape === 's') {update = _execute_straight(token, parseInt(speed));}
    else if (shape === 'k') {update = _execute_kturn(token, parseInt(speed));}
    else if (shape[0] === 'b') {update = _execute_bank(token, parseInt(speed), shape[1]);}
    else if (shape[0] === 't') {update = _execute_turn(token, parseInt(speed), shape[1]);}
    else if (shape[0] === 'n') {update = _execute_sturn(token, parseInt(speed), shape[1]);}
    await token.update(update)
}

function _execute_straight(token, speed){
    //let rotation = ((token.data.rotation+90)%360) * Math.PI/180;
    let rotation = Math.toRadians(token.rotation + 90)
    let ray = Ray.fromAngle(token.x, token.y, rotation, (1+speed)*token.parent.grid.size);
    console.log(token, speed, rotation, ray)
    return {...ray.B};
    //await token.update({...ray.B});
    //await token.parent.updateEmbeddedDocuments('Token', [{_id:token.id, ...ray.B}]);
}

function _execute_bank(token, speed, direction){
    let distance = [2.45, 3.41, 4.37][speed-1];
    let increment = direction === 'r' ? 22.5 : -22.5;
    //let rotation = ((token.data.rotation+90+increment)%360) * Math.PI/180;
    let rotation = Math.toRadians(token.rotation + 90+increment)
    let ray = Ray.fromAngle(token.x, token.y, rotation, distance*token.parent.grid.size);
    return {rotation: token.rotation+(2*increment), ...ray.B};
    //await token.update({rotation: token.rotation+(2*increment), ...ray.B});
    //await token.parent.updateEmbeddedDocuments('Token', [{_id:token.id, rotation: token.data.rotation+(2*increment), ...ray.B}]);
}

function _execute_turn(token, speed, direction){
    let distance = [1.94, 2.93, 3.89][speed-1];
    let increment = direction === 'r' ? 45 : -45;
    //let rotation = ((token.data.rotation+90+increment)%360) * Math.PI/180;
    let rotation = Math.toRadians(token.rotation + 90+increment)
    let ray = Ray.fromAngle(token.x, token.y, rotation, distance*token.parent.grid.size);
    return {rotation: token.rotation+(2*increment),...ray.B};
    //await token.update({rotation: token.rotation+(2*increment),...ray.B});
    //await token.parent.updateEmbeddedDocuments('Token', [{_id:token.id, rotation: token.data.rotation+(2*increment), ...ray.B}]);
}

function _execute_kturn(token, speed) {
    let update =  _execute_straight(token, speed);
    return {...update, rotation: token.rotation+180};
    //await token.update({rotation: token.rotation+180});
    //await token.parent.updateEmbeddedDocuments('Token', [{_id:token.id, rotation: token.data.rotation+180}]);
}

function _execute_sturn(token, speed, direction){
    let update = _execute_bank(token, speed, direction);
    return {...update, rotation: token.rotation+180};
    //await token.update({rotation: token.rotation+180});
    //await token.parent.updateEmbeddedDocuments('Token', [{_id:token.id, rotation: token.data.rotation+180}]);
}

export async function barrelRoll(token, lr, slide=0) {
    if (token?.document) token = token.document;
    let offset_dir = lr.charAt(0)==='r' ? 180 : 0;
    let size_slide = token?.actor?.data?.size === "large" ? slide*token.parent.grid.size : slide*token.parent.grid.size*0.5 
    let offset = Ray.fromAngle(token.x, token.y, (token.rotation+offset_dir)*Math.PI/180, 2*token.parent.grid.size);
    let frontback_slide = Ray.fromAngle(offset.B.x, offset.B.y, (token.rotation+90)* Math.PI/180, size_slide);
    await token.update({...frontback_slide.B});
}