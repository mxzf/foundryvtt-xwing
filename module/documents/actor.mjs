/**
 * Extend the base Actor document by defining a custom roll data structure which is ideal for the Simple system.
 * @extends {Actor}
 */
export class XwingActor extends Actor {


  /**
   * Lazily obtain a FormApplication instance used to configure this Document, or null if no sheet is available.
   * @type {FormApplication|null}
   * @memberof ClientDocumentMixin#
   */
  get sheet() {
    if (this.type === "ship" && 
          this.system.model === "" && 
          this.system.pilotskill == 1 && 
          this.system.points == 0) {

      const cfg = CONFIG[this.documentName];
      const sheets = cfg.sheetClasses[this.type] || {};
      let test =  sheets["xwing.XwingEditSheet"].cls;
      return new test(this, {editable: this.isOwner});
    }
    return super.sheet;
  }

  /** @override */
  prepareData() {
    // Prepare data for the actor. Calling the super version of this executes
    // the following, in order: data reset (to clear active effects),
    // prepareBaseData(), prepareEmbeddedDocuments() (including active effects),
    // prepareDerivedData().
    super.prepareData();
  }

  /** @override */
  prepareBaseData() {
    // Data modifications in this step occur before processing embedded
    // documents or derived data.
  }

  /**
   * @override
   * Augment the basic actor data with additional dynamic data. Typically,
   * you'll want to handle most of your calculated/derived data in this step.
   * Data calculated in this step should generally not exist in template.json
   * (such as ability modifiers rather than ability scores) and should be
   * available both inside and outside of character sheets (such as if an actor
   * is queried and has a roll executed directly from it).
   */
  prepareDerivedData() {
    const actorData = this;
    //const data = actorData.data;
    const flags = actorData.flags.xwing || {};

    // Make separate methods for each Actor type (character, npc, etc.) to keep
    // things organized.
    //this._prepareCharacterData(actorData);
    //this._prepareNpcData(actorData);
  }

  /**
   * Prepare Character type specific data
   */
  _prepareCharacterData(actorData) {
    if (actorData.type !== 'character') return;

    // Make modifications to data here. For example:
    const data = actorData.data;

    // Loop through ability scores, and add their modifiers to our sheet output.
    for (let [key, ability] of Object.entries(data.abilities)) {
      // Calculate the modifier using d20 rules.
      ability.mod = Math.floor((ability.value - 10) / 2);
    }
  }

  /**
   * Prepare NPC type specific data.
   */
  _prepareNpcData(actorData) {
    if (actorData.type !== 'npc') return;

    // Make modifications to data here. For example:
    const data = actorData.data;
    data.xp = (data.cr * data.cr) * 100;
  }

  /**
   * Override getRollData() that's supplied to rolls.
   */
  getRollData() {
    const data = super.getRollData();

    // Prepare character roll data.
    this._getCharacterRollData(data);
    this._getNpcRollData(data);

    return data;
  }

  /**
   * Prepare character roll data.
   */
  _getCharacterRollData(data) {
    if (this.type !== 'character') return;

    // Copy the ability scores to the top level, so that rolls can use
    // formulas like `@str.mod + 4`.
    if (data.abilities) {
      for (let [k, v] of Object.entries(data.abilities)) {
        data[k] = foundry.utils.deepClone(v);
      }
    }

    // Add level for easier access, or fall back to 0.
    if (data.attributes.level) {
      data.lvl = data.attributes.level.value ?? 0;
    }
  }

  /**
   * Prepare NPC roll data.
   */
  _getNpcRollData(data) {
    if (this.type !== 'npc') return;

    // Process additional NPC data here.
  }

  applyDamage(damage={'hits':0}){
    if (!this?.parent?.combatant) return ui.notifications.info("Can't apply damage to a ship that's not in combat");

    if (damage?.evade){
      let canceled_hits = Math.min(damage.evade, damage.hits);
      damage.evade -= canceled_hits;
      damage.hits -= canceled_hits;
      if (damage.evade > 0 && damage?.crits > 0){
        let canceled_crits = Math.min(damage.evade, damage.crits);
        damage.evade -= canceled_crits;
        damage.crits -= canceled_crits;
      }
    }
    if (damage.hits > 0){
      let shield_damage = Math.min(damage.hits, this.system.shields.value);
      damage.hits -= shield_damage;
      this.system.shields.value -= shield_damage;
    }
    if (this.system.shields.value > 0 && damage?.crits > 0){
      let shield_crits = Math.min(damage.crits, this.system.shields.value);
      damage.crits -= shield_crits;
      this.system.shields.value -= shield_crits;
    }
    if (damage.hits > 0){
      let hull_damage = Math.min(damage.hits, this.system.hull.value);
      damage.hits -= hull_damage;
      this.system.hull.value -= hull_damage;
    }
    if (this.system.hull.value > 0 && damage?.crits > 0){
      let hull_crits = Math.min(damage.crits, this.system.hull.value);
      damage.crits -= hull_crits;
      this.system.hull.value -= hull_crits;
      ui.notifications.info(`Suffered ${hull_crits} crits`);
    }
  }
}