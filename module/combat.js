import {executeManeuver} from './helpers/maneuvers.mjs';

export class XwingCombat extends Combat {
    constructor(...args) {
        super(...args);
        //this.setFlag('xwing', 'phase', 'move');
        //this.flags.xwing.phase = "move";
    }
    get phase() {
        return this.getFlag("xwing", "phase") || "assign";
    }
    
    async nextRound() {
        if ( this.phase === "move" ) {
          let to_return = await this.update({"flags.xwing.phase": "shoot", turn: 0});
          this.render();
          return to_return;
        }
        else {
          await this.setFlag("xwing", "phase", "assign"); // Back to the assign phase
          return super.nextRound()
        } 
    }

    nextTurn(options){
        if (this.phase === "assign"){
            return this.update({"flags.xwing.phase": "move"});
        }
        return super.nextTurn(options);
    }

    _sortCombatants(a, b) {
        let phase = a.combat.phase;
        if ( phase == "shoot" ) return super._sortCombatants(a, b); // Reverse order during shoot phase
        else return super._sortCombatants(b, a);
    }

    _onUpdate(data, options, userId) {
        super._onUpdate(data, options, userId);
        if ( foundry.utils.hasProperty(data, "flags.xwing.phase") ) this.setupTurns();
    }
}

export class XwingCombatTracker extends CombatTracker {
    constructor(options) {
        super(options);
    }

    /** @inheritdoc */
	static get defaultOptions() {
        return foundry.utils.mergeObject(super.defaultOptions, {
        id: "combat",
        template: "systems/xwing/templates/combat-tracker.html",
        title: "Combat Tracker",
        scrollY: [".directory-list"]
      });
    }

    /** @inheritdoc */
    async getData(options) {
        const context = await super.getData(options);
        context.icons = CONFIG.XWING.icons;

        context.turns.forEach( turn => {
            turn.flags = context.combat.combatants.get(turn.id)?.flags;
            turn.model = context.combat.combatants.get(turn.id)?.actor.system.model;
            turn.tint = context.combat.combatants.get(turn.id)?.token.tint ?? false;
        });
        
        context.phase_label = game.i18n.localize(`XWING.phases.${context.combat?.phase}`) ?? "";
        context.phase = context.combat?.phase ?? "none";
        return context;
    }

    activateListeners(html) {
        super.activateListeners(html);

        html.find('.assign-maneuver').click(async ev => {
            ev.stopPropagation();
            const combatant_id = $(ev.currentTarget).parents(".combatant").data("combatantId");
            new game.xwing.ManeuverSelection(this.viewed.combatants.get(combatant_id),"assign").render(true);
        });

        html.find('.execute-maneuver').click(async ev => {
            ev.stopPropagation();
            const combatant_id = $(ev.currentTarget).parents(".combatant").data("combatantId");
            let combatant = this.viewed.combatants.get(combatant_id);
            await executeManeuver(combatant.token, combatant.flags.xwing.maneuver)
            await combatant.unsetFlag("xwing", "maneuver");
        });

        html.find(".dont-execute").click(async ev => {
            ev.stopPropagation();
            ui.notifications.info("Can not execute maneuver because it is not that token's turn");
        });
    }
}