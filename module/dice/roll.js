export class RollXWing extends Roll {
    constructor(...args) {
        super(...args);
    }
    /**
     * The HTML template path used to render a complete Roll object to the chat log
     * @type {string}
     */
    static CHAT_TEMPLATE = "systems/xwing/templates/dice/roll.html";

    /**
     * The HTML template used to render an expanded Roll tooltip to the chat log
     * @type {string}
     */
    static TOOLTIP_TEMPLATE = "systems/xwing/templates/dice/tooltip.html";

    /** @override */
    async render(chatOptions={}) {
        chatOptions = foundry.utils.mergeObject({
          user: game.user.id,
          flavor: null,
          template: this.constructor.CHAT_TEMPLATE,
          blind: false
        }, chatOptions);
        const isPrivate = chatOptions.isPrivate;
    
        // Execute the roll, if needed
        if (!this._evaluated) this.evaluate();
    
        // Define chat data
        const chatData = {
          formula: isPrivate ? "???" : this._formula,
          flavor: isPrivate ? null : chatOptions.flavor,
          user: chatOptions.user,
          tooltip: isPrivate ? "" : await this.getTooltip(),
          total: isPrivate ? "?" : Math.round(this.total * 100) / 100
        };
    
        // Render the roll display template
        return renderTemplate(chatOptions.template, chatData);
      }

    /** @override */
    async toMessage(messageData={}, {rollMode, create=true}={}) {

        // Perform the roll, if it has not yet been rolled
        if (!this._evaluated) await this.evaluate({async: true});
    
        // Prepare chat data
        messageData = foundry.utils.mergeObject({
          user: game.user.id,
          type: CONST.CHAT_MESSAGE_TYPES.ROLL,
          content: this.total,
          sound: CONFIG.sounds.dice,
        }, messageData);
        messageData.roll = this;
    
        // Either create the message or just return the chat data
        const cls = getDocumentClass("ChatMessage");
        const msg = new cls(messageData);
        if ( rollMode ) msg.applyRollMode(rollMode);
    
        // Either create or return the data
        if ( create ) return cls.create(msg, { rollMode });
        else return msg;
      }
}