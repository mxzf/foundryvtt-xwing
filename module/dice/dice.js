export class AttackDie extends Die {
    constructor(termData){
        termData.faces=8;
        super(termData);
    }
    /** @override */
    static DENOMINATION = "a";

    static face_labels = ['','','f','f','d','d','d','c'];

    /** @override */
    getResultLabel(result) {
        return ['&nbsp;','&nbsp;','f','f','d','d','d','c'][result.result-1];
    }

    getResultCSS(result) {
        return [
            this.constructor.name.toLowerCase(),
            "xwing-icon"
        ]
    }
}

export class DefenseDie extends Die {
    constructor(termData){
        termData.faces=8;
        super(termData);
    }
    /** @override */
    static DENOMINATION = "d";

    static face_labels = ['','','','f','f','e','e','e'];

    /** @override */
    getResultLabel(result) {
        return ['&nbsp;','&nbsp;','&nbsp;','f','f','e','e','e'][result.result-1];
    }

    getResultCSS(result) {
        return [
            this.constructor.name.toLowerCase(),
            "xwing-icon"
        ]
    }
}