## 0.1.3 (2023-03-27)

* Updated CI process to make properly installable links

## 0.1.2 (2022-11-05)

### Fixes

* Updated to use new Foundry V10 syntax for system data

### Features

* Updated token movement to do a smooth move+rotate instead of the previous move and then rotate animations


## 0.1.1 (2022-01-04)

### Fixes

* Changed the "execute maneuver" in the combat tracker to prevent tokens from moving out of turn


## 0.1.0 (2021-12-31)

### Features

* X-Wing specific dice rolling, with `Nda` for rolling N attack dice and `Ndd` for rolling N defense dice
* Actor sheets with basic functionality
    * Automatic point total calculation based on upgrades
    * Buttons for Focus/Evade/Boost/Barrel Roll actions (if the ship has those actions)
    * Counters for various tokens like stress/focus/evade
    * Upgrade slot handling with counts
* Secondary actor sheet for editing actors with the key functionality supported (still needs lots of polish)
    * Assign ship type, attack/agility/etc values
    * Add/remove upgrade slots
    * Toggle action bar actions
    * Configure the available maneuvers
* Combat tracker modified to handle X-Wing Minis combat order
    * Handles Assign/Move/Attack phases and re-sorting initiative as-needed
* Maneuver selection dialog that can select and execute maneuvers
