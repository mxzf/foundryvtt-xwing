Images in the images/game-icons/ folder licensed under CC BY 3.0 and downloaded from game-icons.net

Individual images by:
stealth-bomber.png: Delapouite
interceptor-ship.png: Delapouite
scout-ship.png: Delapouite
starfighter.png: Delapouite
spaceship.png: Delapouite
bubble-field.png: Lorc
forward-field.png: Lorc
